trulla
=
This is your personal assistant for scheduling recurring tasks on trello without having trello gold.
The program can also archive all cards in a given list, for a specified board.
It is intended to be run as a cron job.

Usage
-
Run it as ```./trulla.py __conf_dir___```.
Here ```__conf_dir__``` is a directory containing three files.

1) One called ```api_key```, containing your [API key](https://trello.com/app-key).

2) One called ```token```, containing the token you generated for manual testing, as described at [https://trello.com/app-key](https://trello.com/app-key).

3) A file called ```cards.toml``` containing the description of the recurring tasks in [toml](https://github.com/toml-lang/toml) format.


Definition of Recurring Tasks
-
To specify a recurring card, you need to state the board and list where it should be placed.
This is done by defining an array of tables called ```board```, having a `name` field.
```toml
[[board]]
   name="board_name"
```
Optionally you can also specify a `done` and a `freq`field.
All cards in the list named in the `done` field are archived with the frequency given in the `freq` field.
The frequency can be one of `"w"` for weekly, `"m"` for monthly, `"y"` for yearly or `"a"` for always.

Now you need to give the lists, where the cards have to be placed.
Again this is an array of tables.
```toml
[[board.list]]
    name="list_name"
```

Now you can define a card as another nested array of tables.
You need to give a name for the card, a frequency and a due date.
Optional are a description and the name of a single label.
```toml
[[board.list.card]]
    name="card_name"
    desc="description of the task"
    label="label_name"
    freq="w"
    due=24
```
Again, `freq` can be one of `"w"` for weekly, `"m"` for monthly, `"y"` for yearly or `"a"` for always.
For weekly, monthly and always frequencies, he `due` field gives the due date of the card as the number of days from the start of the current, week, month or today respectively.
The yearly frequency expects a string with the day and month separated by `-`.


A new recurring card in the same board can be defined by a new table in the same array.
This is done, by again stating `[[board.list.card]]`.
Simmilarily a  new list in the same board as the previous list can be described by again stating
```toml
[[board.list]]
    name="another_list"
```

Additional boards can be refrenced by additional tables in the array.
```toml
[[board]]
    name="next_board"
```

Restrictions
-
* For now you can not have two boards with the same name.
* Also, no board can have two lists with the same name
* Neither can there be two labels with the same, not empty, name.
* Recurring cards can not have more than one label.