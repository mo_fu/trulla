import os
import sys
import time
import datetime
from datetime import datetime as dt
from datetime import date
from datetime import timedelta as delta
import toml
import due as dued

format="%d-%m-%Y"

def readCards(cardpath):
    return toml.load(cardpath)

def getLasttime(cachepath):
    try:
        with open(os.path.join(cachepath,"lasttime")) as f:
            dateline = f.readline()
            try:
                return dt.strptime(dateline,format)
            except ValueError:
                pass
    except FileNotFoundError:
        pass
    return dt.min

def setLasttime(cachepath):
    path=os.path.join(cachepath,"lasttime")
    try:
        f=open(path,'w')
    except FileNotFoundError:
        os.makedirs(cachepath,mode=0o750)
        f=open(path,'w')

    f.write(date.today().strftime(format))
    f.close()

def getRecurringCards(cards, lasttime):
    ret_dict=dict()
    for board in cards["board"]:
        try:
            boardName= board["name"]
        except KeyError:
            sys.stderr.write("There is a board with no name.")
            continue
        done_list=board.get("done",None)
        brd_freq=board.get("freq",None)
        ret=[]
        for l in board["list"]:
            try:
                listName=l["name"]
            except KeyError:
                sys.stderr.write("There is a list with no name.")
                continue
            for card in l["card"]:
                dueDate=computeDueDate(lasttime,card["due"],card["freq"])
                if dueDate is not None:
                    dueTime=dt.combine(dueDate,datetime.time(23,59,59))\
                            +delta(0,time.timezone)
                    card["due"]=dueTime.isoformat(timespec='milliseconds')+'Z'
                    card.pop("freq")
                    card["listName"]=listName
                    ret.append(card)
        cs,dl,fq=ret_dict.get(boardName,([],None,None))
        if done_list is not None:
            dl=done_list
        if brd_freq is not None:
            fq=brd_freq
        cs=cs+ret
        ret_dict[boardName]=(cs,dl,fq)
    return ret_dict

def computeDueDate(lasttime,dueDate,freq):
    due=dued.due(freq)
    if due.predicate(lasttime):
        return due.dueDate(dueDate)
    return None



