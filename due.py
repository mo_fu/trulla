from datetime import date


class Due:
    def __init__(self):
        self.today = date.today()

    def predicate(self,lasttime):
        return True

    def dueDate(self,due):
        self.today()


class DueY(Due):

    def __init__(self):
        super().__init__()
    def predicate(self,lasttime):
        return lasttime.year < self.today.year

    def dueDate(self,due):
        day,month=due.split('-')
        return self.today.replace(month=int(month),day=int(day))


class DueM(DueY):

    def __init__(self):
        super().__init__()

    def predicate(self,lasttime):
        return lasttime.month < self.today.month or super().predicate(lasttime)

    def dueDate(self,due):
        return self.today.replace(day=int(due))


class DueW(DueY):

    def __init__(self):
        super().__init__()

    def predicate(self,lasttime):
        lasttime_week=lasttime.isocalendar()[1]
        this_week=self.today.isocalendar()[1]
        return lasttime_week < this_week or super().predicate(lasttime)

    def dueDate(self,due):
        return self.today.replace(day=self.today.day-self.today.weekday()+int(due))


def due(c):
    c=c.lower()
    if c == 'w':
        return DueW()
    if c == 'm':
        return DueM()
    if c== 'y':
        return DueY()
    if c=="a":
        return Due()
    raise FrequencyException()


class FrequencyException(Exception):
    def __init__(self):
        super("Invalid frequency for reccuring Element")

