import requests as req
import board

api_root="https://trello.com/1"


class GetException(Exception):
    def __init__(self, reason):
        self.message= "Could not get "+reason


class PostException(Exception):
    def __init__(self, reason):
        self.message= "Could not post "+reason


class DuplicateException(Exception):
    def __init__(self, reason):
        self.message="There is more than one "+reason+" with the same name."


class Trello:
    def __init__(self,auth):
        self.auth=auth

    def boards(self):
        response=req.get(api_root+"/members/me/boards",params=self.auth)
        if not response.ok:
            raise GetException("boards")
        boards=dict()
        for brd in response.json():
            boardName=brd["name"]
            if boardName in boards:
                raise DuplicateException("board")
            boards[boardName]=board.Board(brd,self.lists(brd),self.labels(brd))
        return boards

    def lists(self,board):
        response = req.get('/'.join([api_root, "boards", board["id"], "lists"]),
                           params=self.auth)
        if not response.ok:
            raise GetException("lists for board "+board["name"])
        lists=dict()
        for l in response.json():
            listName=l["name"]
            if listName in lists:
                raise DuplicateException("list on board "+board["name"])
            lists[listName] = l
        return lists

    def labels(self,board):
        response = req.get('/'.join([api_root, "boards", board["id"], "labels"]),
                           params=self.auth)
        if not response.ok:
            raise GetException("labels for board "+board["name"])
        lists=dict()
        for l in response.json():
            listName=l["name"]
            if listName == "":
                # There may be mepty labels, we do not want them considered.
                # Otherwise there is trouble because of duplicates.
                continue
            if listName in lists:
                raise DuplicateException("label on board "+board["name"])
            lists[listName] = l
        return lists

    def add_card(self, board, list, name, desc="", due=None):
        params={"name":name, "desc":desc}
        if not due is None:
            params["due"]=due
        params=params+self.auth
        response=req.post(api_root+"/cards",params=params)
        if not response.ok:
            raise PostException("card "+name)

    def post_cards(self, cards):
        for c in cards:
            c.update(self.auth)
            response=req.post(api_root+"/cards",params=c)
            # uncomment for testing. The created card will be deleted immediately.
            # watch your board closely the appear for only a second.
            #if response.ok:
            #    json=response.json()
            #    cardId=json["id"]
            #    req.delete(api_root+"/cards/"+cardId,params=self.auth)
            #else:
            #    print(response.content)

    def archive_list_elements(self, list_id):
        response=req.get('/'.join([api_root,"lists",list_id,"cards"]),params=self.auth)
        if response.ok:
            params={"value":"true"}
            params.update(self.auth)
            jsons=response.json()
            if not isinstance(jsons,list):
                jsons=(jsons,)
            for c in jsons:
                response=req.put('/'.join([api_root,"cards",c["id"],"closed"]),params=params)
