import due

class Board:

    def __init__(self, boardSpec, lists, labels):
        self.boardSpec=boardSpec
        self.lists=lists
        self.labels=labels

    def names_to_ids(self, cards):
        for c in cards:
            try:
                label_name=c.pop("label")
                label_id=self.labels[label_name]["id"]
                c["idLabels"]=label_id
            except KeyError:
                pass
            try:
                list_name=c.pop("listName")
                list_id=self.lists[list_name]["id"]
                c["idList"]=list_id
            except KeyError:
                pass
            c["idBoard"]=self.boardSpec["id"]

    def get_list_id(self, list_name,lasttime, freq):
        du= due.due(freq)
        if du.predicate(lasttime):
            try:
                return self.lists.get(list_name,None)["id"]
            except TypeError:
                pass
        return None