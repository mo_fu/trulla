#!/usr/bin/python3
import os
import os.path as path
import sys
import toml

import trello
import cards
import due


class Task:
    def __init__(self,title,description="",date=None,labels=[]):
        self.title=title
        self.description=description
        self.date=date
        self.labels=labels

if __name__=="__main__":
    confpath=sys.argv[1]
    confpath=path.expanduser(confpath)
    with open(path.join(confpath,"token")) as f:
        token = f.readline()
    if token == None:
        sys.stderr.write("Could not read api token")
        sys.exit(1)
    with open(path.join(confpath,"api_key")) as f:
        key = f.readline()
    if key == None:
        sys.stderr.write("Could not read api_key")
        sys.exit(1)
    auth = {"key": key, "token": token}
    q = trello.Trello(auth)
    boards=q.boards()
    lastTime=cards.getLasttime(path.expanduser("~/.cache/trulla"))
    cs=toml.load(path.join(confpath,"cards.toml"))
    conf_dict=cards.getRecurringCards(cs,lastTime)
    for bName, trip in conf_dict.items():
        board = boards.get(bName)
        cs,done_list,freq=trip
        if board is not None:
            board.names_to_ids(cs)
            q.post_cards(cs)
            if done_list is not None:
                list_id=board.get_list_id(done_list,lastTime,freq)
                if list_id is not None:
                    q.archive_list_elements(list_id)

    cards.setLasttime(path.expanduser("~/.cache/trulla"))

